package cn.xsshome.body;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;

import javax.imageio.ImageIO;

import cn.xsshome.body.util.Base64Util;
import cn.xsshome.body.util.HttpUtil;
import cn.xsshome.body.util.VideoFrame2ImageUtil;

/**
 * 视频帧转图片请求人流量统计（动态版）接口
 * @author 小帅丶
 *
 */
public class BodyTrackingAPISample {
	//人流量统计（动态版）接口地址
	private static String BODY_TRACKING_URL="https://aip.baidubce.com/rest/2.0/image-classify/v1/body_tracking";
	public static void main(String[] args) throws Exception {
		String access_token = "";
		String videoPath = "G:/testvideo/test.mp4";
		BufferedImage bufferedImage = VideoFrame2ImageUtil.getVideoFramer4bufferedImage(videoPath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", baos);
		//转为base64
		String base64 = Base64Util.encode(baos.toByteArray());
		String params = "image="+URLEncoder.encode(base64,"utf-8")+"&dynamic=false&show=true";
		String result = HttpUtil.post(BODY_TRACKING_URL, access_token, params );
		System.out.println(result);
	}
}
